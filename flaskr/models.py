from flask_sqlalchemy import SQLAlchemy
from flask.cli import with_appcontext
from bcrypt import gensalt, hashpw, checkpw
from click import command, echo


# `db` defines our connection to the database and is referenced anywhere where
# we need to be saving or retrieving data from the database.
db = SQLAlchemy()


@command("init-db")
@with_appcontext
def init_db_command():  # pragma: no cover
    """Clear and re-initialize the database. The decorators on this function ensure that
    we run this function inside of the app context and allow it to be run from the
    command line with `flask init-db`."""

    db.drop_all()
    db.create_all()
    echo("Database initialized.")


class Base(object):
    """This model is used for inheritance. We don't want to actually have a table in our
    database called `base`, so we don't inherit from `db.Model`, but we do want to define
    a primary key field that all of the other models we define will have access to. If we
    wanted to add any other fields that all of our models shared, we could do that here.
    """

    id = db.Column(db.Integer, primary_key=True)


class User(Base, db.Model):
    """Define our user model in the database. We need to store the username and a hash of
    the user's password."""

    username = db.Column(db.String(20), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)

    def hash_pw(pw):
        """Salt and hash a password so that it can be checked, but not reversed or brute
        forced."""

        return hashpw(pw.encode(), gensalt()).decode()

    def check_pw(self, pw):
        """Check if a user's password is correct."""

        return checkpw(pw.encode(), self.password.encode())


class AuthToken(Base, db.Model):
    """This model will be the authorization tokens for our users. They are stored here in
    plaintext, which if we wanted to be really secure we could encrypt them in case of
    database leak, and linked to the users with an expiry. When a user logs in, they're
    provided with a token that they can then use to authorize themselves in later
    requests. This is a trusted and established authentication method, but does require
    that TLS or some other encryption is used to prevent eavesdroppers from acquiring the
    token."""

    user = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    token = db.Column(db.String(32), nullable=False, unique=True)
    expiry = db.Column(db.DateTime, nullable=False)


class History(Base, db.Model):
    """This model will store a history for our users. As users make requests, we'll store
    the request, the time, the inputs, and the outputs."""

    user = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    function = db.Column(db.String(10), nullable=False)
    time = db.Column(db.DateTime, nullable=False)
    input = db.Column(db.String(256), nullable=True)
    output = db.Column(db.String(256), nullable=True)
